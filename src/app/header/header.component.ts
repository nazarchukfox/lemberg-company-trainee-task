import { Component, HostListener } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent {
  private maxMenuWidth = 768;
  public isOpened: boolean = false;
  public menu = [
    {name: 'Home', href: '#'},
    {name: 'What we do', href: '#'},
    {name: 'About us', href: '#'}
    ];
  public mobileMenu = {main: [
    ...this.menu,
    {name: 'Our history', href: '#'},
    {name: 'Contact us', href: '#contact'},
    ],
    services: [
      {name: 'Training and Implementation', href: '#'},
      {name: 'Cyber Consultants', href: '#'},
      {name: 'Consultancy and brand protection', href: '#'},
      {name: 'Software', href: '#contact'},
    ]
  };

  toggle() {
    this.isOpened = !this.isOpened;
  }

  @HostListener('window:resize')
  resize() {
    if(document.body.clientWidth > this.maxMenuWidth && this.isOpened) {
      this.isOpened = false;
    }
  }
}
