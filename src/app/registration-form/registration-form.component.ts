import { Component, HostBinding, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder, Validators, AbstractControl, ValidationErrors } from '@angular/forms';


/**
 * Phone number regular expression.
 * Valid number that contains at least 5 numbers (or more).
 * Accepts next number formats:
 * - +380960231221
 * - +38-096-02-31-221
 * - +38-(096)-02-31-221
 * - +38(096)0231221
 * - +380960231221
 * - +38 096 02 31 221
 * - +38 (096) 02 31 221
 * - 380960231221
 * - 38094
 */
const COMPLEX_PHONE_NUMBER_REGEX = /^\+?\d{1,4}?[-.\s]?\(?\d{1,3}?\)?[-.\s]?\d{1,4}[-.\s]?\d{1,4}[-.\s]?\d{1,9}$/;

@Component({
  selector   : 'app-registration-form',
  templateUrl: './registration-form.component.html',
  styleUrls  : ['./registration-form.component.scss']
})

export class RegistrationFormComponent {
  readonly getErrorMessage = getErrorMessage;

  form: FormGroup = this.fb.group({
    name   : ['', [
      Validators.required,
      Validators.minLength(2),
      Validators.maxLength(60)
    ]],
    company: ['', [Validators.required,
      Validators.maxLength(120)]],
    phone  : ['', [Validators.required,
      Validators.pattern(COMPLEX_PHONE_NUMBER_REGEX)]],
    email  : ['', [Validators.required,
      Validators.email]],
    message: ['', [Validators.required,
      Validators.maxLength(200)]]
  });

  constructor(private fb: FormBuilder) {
  }

  onSubmit() {
    const controls = this.form.controls;
    if (this.form.invalid) {
      Object.keys(controls)
        .forEach(controlName => controls[controlName].markAsTouched());
      return;
    }

    return this.form.reset();
  }

  isControlInvalid(control: AbstractControl | null): boolean {
    if (control) {
      return control.touched && control.invalid;
    }
    return false;
  }
}

function getErrorMessage(errors: ValidationErrors | null | undefined, name: string): string {
  if (errors) {
    const type = Object.keys(errors)[0];
    switch (type) {
      case 'required':
        return `${name} is required`;
      case 'maxlength':
      case 'minlength':
      case 'pattern':
      case 'email':
        return `${name} is invalid`;
    }
  }
  return '';
}
